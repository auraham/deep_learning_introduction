# Notes



## Use Jupyter notebook remotely

**Server**

```
jupyter notebook --no-browser --port=8889
```

Alternatively:

```
nohup jupyter notebook --no-browser --port=8889 &
```

**Local computer**

Redirect ports:

```
ssh -N -f -L localhost:8888:localhost:8889 username@your_remote_host_name
```

Open this address in your browser:

```
localhost:8888
```

[Use Jupyter notebook remotely](https://amber-md.github.io/pytraj/latest/tutorials/remote_jupyter_notebook)
[Running a Jupyter notebook from a remote server](https://ljvmiranda921.github.io/notebook/2018/01/31/running-a-jupyter-notebook/)






## Displaying images


Consider this snippet:

```python
# test_model.py
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from keras.preprocessing import image
import os

if __name__ == "__main__":
    
    test_dir_dogs = "/media/data/dogs_vs_cats_small/test/dogs"
    fnames = [os.path.join(test_dir_dogs, fname) for fname in os.listdir(test_dir_dogs)]

    img_path = fnames[4]
    img = image.load_img(img_path)
    x = image.img_to_array(img)
    
    plt.imshow(x)
```

You will get this error when trying to display an image:

```
W1007 12:11:58.658936 140436321801984 image.py:648] Clipping input data to the valid range for imshow with RGB data ([0..1] for floats or [0..255] for integers).
```

That is because the `dtype` of the image `x` is not correct. In this case, the image is converted from `PIL` to `np.array` as follows:

```python
# incorrect casting
img = image.load_img(img_path)
x = image.img_to_array(img)
```

However, if we inspect `x`, we notice that it is a `float32` array but its range is not [0, 1]:

```
In [34]: x.dtype
Out[34]: dtype('float32')

In [35]: x[:4, :4, 0]        
Out[35]: 
array([[225., 226., 229., 232.],
       [215., 216., 217., 219.],
       [209., 209., 208., 208.],
       [212., 211., 209., 208.]], dtype=float32)
```

From the [documentation](https://matplotlib.org/3.1.1/api/_as_gen/matplotlib.pyplot.imshow.html), `plt.imshow` expects one of these options:

- `(M, N)`: an image with scalar data.
- `(M, N, 3)`: an image with RGB values **(0-1 float or 0-255 int)**.
- `(M, N, 4)`: an image with RGBA values (0-1 float or 0-255 int), including transparency.

Our image is does not fit on any of these options. We can either change the type or the range (via normalization).  Let's check both methods:

```python
# convert from PIL to np.array (cast to np.int in the range [0, 255])
img_path = fnames[4]
img = image.load_img(img_path)
x = image.img_to_array(img).astype(int)
plt.figure()
plt.imshow(x)
```

```python
# convert rfrom PIL to np.array (as np.float32 in the range [0, 1])
img_path = fnames[5]
img = image.load_img(img_path)
x = image.img_to_array(img) * 1./255
plt.figure()
plt.imshow(x)
```

Just for completeness, here is the full code:

```python
# test_model.py
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from keras.preprocessing import image
import os

if __name__ == "__main__":
    
    test_dir_dogs = "/media/data/dogs_vs_cats_small/test/dogs"
    fnames = [os.path.join(test_dir_dogs, fname) for fname in os.listdir(test_dir_dogs)]

    # convert from PIL to np.array (cast to np.int in the range [0, 255])
    img_path = fnames[4]
    img = image.load_img(img_path)
    x = image.img_to_array(img).astype(int)
    plt.figure()
    plt.imshow(x)
    
    # convert from PIL to np.array (as np.float32 in the range [0, 1])
    img_path = fnames[5]
    img = image.load_img(img_path)
    x = image.img_to_array(img) * 1./255
    plt.figure()
    plt.imshow(x)
    
    plt.show()
```


Here is the result:

![](img/fig_dog_1.png)

![](img/fig_dog_2.png)



## Using `ImageDataGenerator`

Consider this snippet:

```python
from keras.preprocessing.image import ImageDataGenerator

test_dir = "/media/data/dogs_vs_cats_small/test/dogs"
test_datagen = ImageDataGenerator(rescale=1./255)
test_generator = test_datagen.flow_from_directory(test_dir, 
                                                  target_size=(150, 150),
                                                  batch_size=32,
                                                  class_mode="binary"
                                                  )
# print the first batch
for batch in test_generator:
    print(batch)
	break
```

If you run it, you will get this error:

```
Found 0 images belonging to 0 classes.
```

[It turns out](https://github.com/keras-team/keras/issues/3946) that this error is thrown out when the path of the images (`test_dir` in this case), does not have the structure expected by Keras. In this example, `test_dir` contains images of dogs only. However, the content of `test_dir` should be divided into directories, one for each class:

```
dogs_vs_cats_small/
	test/
		dogs/		# class 1
		cats/		# class 2
	train/
		dogs/		# class 1
		cats/		# class 2
	validation/
		dogs/		# class 1
		cats/		# class 2						
```

Therefore, just change `test_dir` as follows to fix it:

```python
test_dir = "/media/data/dogs_vs_cats_small/test/dogs"
```

Now, you will get this message:

```
Found 1000 images belonging to 2 classes.
```

This is the full code for completeness:

```python
from __future__ import print_function
from keras.preprocessing.image import ImageDataGenerator

if __name__ == "__main__":
    
    test_dir = "/media/data/dogs_vs_cats_small/test"
    test_datagen = ImageDataGenerator(rescale=1./255)
    test_generator = test_datagen.flow_from_directory(test_dir, 
                                                    target_size=(150, 150),
                                                    batch_size=32,
                                                    class_mode="binary"
                                                    )
    # print the first batch
    for batch in test_generator:
        print(batch)
        break
```



## Using `fit` and `fit_generator`

You can specify the training samples (`traing_features`) and their labels (`train_labels`) in `fit` as follows:

```python
# Listing 5.18 Defining and training the densely connected classifier
history = model.fit(train_features,
                    train_labels,
                    batch_size=20,
                    validation_data=(validation_features, validation_labels))
```

You can also employ a generator (`ImageDataGenerator`) to provide the training samples and labels (`train_generator`):

```python
# Listing 5.21 Training the model end to end with a frozen convolutional base
history = model.fit_generator(train_generator,
                              steps_per_epoch=100,
                              epochs=30,
                              validation_generator=validation_generator,
                              validation_steps=50)
```

From the above snippets, it seems that the `fit` function depends on how the images are given:

- If the images and labels are given as two individual arrays (e.g., `train_features` and `train_labels`), use `fit`.
- If the images and labels are given as a generator (e.g., `train_generator`), use `fit_generator`.

Pretty obvious, right? Well, after checking my code, I found this:

```python
# Listing 5.8 Fitting the model using a batch generator
history = model.fit(                               # my mistake: fit instead of fit_generator
            train_generator,
            steps_per_epoch=100,
            epochs=30,
            validation_data=validation_generator,
            validation_steps=50
            )
```

I used `fit` instead of `fit_generator`. However, the code worked as expected. According to the [documentation](https://keras.io/models/sequential/) (shorten for clarity):

> ```
> fit(x=None, y=None, steps_per_epoch=None, validation_data=None, validation_steps=None, ...)
> ```
> `x` can be:
>
> - A Numpy array (or array-like)
> - A generator or `keras.utils.Sequence` returning `(inputs, targets)` or `(inputs, targets, sample _weights)`
>
> If `x` is a generator, or `keras.utils.Sequence` instance, `y` should not be specified (since targets will be obtained from `x`). 



> ```
> fit_generator(generator, steps_per_epoch=None, validation_data=None, validation_steps=None, ...)
> ```
>
> - `generator` A generator or an instance of `keras.util.Sequence`
> - `steps_per_epoch` Total number of steps before stopping the `yield` loop of `generator`. It should typically be equal to `ceil(num_samples/batch_size)`.
> - `epochs` Number of epochs to train the model.
> - `validation_data` this can be either
>   - a generator or a `Sequence` object
>   - tuple `(x_val, y_val)`
>   - tuple `(x_val, y_val, val_sample_weights)`
> - `validation_steps` only relevant if `validation_data` is a generator. Total number of steps (batches of samples) to yield from `validation_data` before stopping at the end of every epoch. It should typically be equal to `ceil(num_val_samples/batch_size)`.

That is, it seems that both functions, `fit` and `fit_generator` accept a generator as input. However, `fit_generator` does not accept `x` and `y` as input like `fit` does. Anyway, I am not completely sure if you should use them interchangeably because the meaning of `steps_per_epoch` and `validation_steps` differs in `fit` and `fit_generator`:



> ```
> fit(steps_per_epoch=None, validation_steps=None, ...)
> ```
>
> - `steps_per_epoch`: Total number of steps (batches of samples) before declaring one epoch finished and starting the next epoch.  (**i.e., it did not mention that this parameter is only relevant if `x` is a generator**)
> - `validation_steps`: Only relevant if `steps_per_epoch` is specified. Total number of steps (batches of samples)
>   to validate before stopping.
> - `validation_steps`: Only relevant if `validation_data` is provided and is a generator. Total number of steps (batches of samples) to draw before stopping when performing validation at the end of every epoch. (**this is similar to `validation_steps` in `fit_generator`**)

Although these parameters, `steps_per_epoch` and `validation_steps`, can be employed in `fit` and `fit_generator`, their meanings seem to differ. For this reason, I think that :

1. You should use `fit_generator` when the training samples and labels are given as a generator. If so, do not forget to specify `steps_per_epoch=ceil(num_samples/batch_size)`. The same rule applies for `validation_data`. If it is a generator, use `validation_steps=ceil(num_val_samples/batch_size)`.
2. You should use `fit` is the training samples and labels are stored as two arrays, `x` and `y`. If so, you should consider to specify a `batch_size` (otherwise, the default value will be used, `batch_size=32`).




## Constructing your network according to the type of problem

In [1] page 114, the author gives the following table to help you chose the right last-layer activation and loss function for your model.

| Problem type                            | Last-layer activation | Loss function                  | Example                                                      |
| --------------------------------------- | --------------------- | ------------------------------ | ------------------------------------------------------------ |
| Binary classification                   | `sigmoid`             | `binary_crossentropy`          | Binary classification of movie reviews from the IMDB dataset, page 68. |
| Multiclass, single-label classification | `softmax`             | `categorical_crossentropy`     | Classification of news into 46 mutually exclusive topics, page 78. |
| Multiclass, multi-label classification  | `sigmoid`             | `binary_crossentropy`          | None                                                         |
| Regression to arbitrary values          | None                  | `mse`                          | Prediction of house prices, page 85.                         |
| Regression to values between 0 and 1    | `sigmoid`             | `mse` or `binary_crossentropy` | None                                                         |

From [2] page 276:

> **Activation functions**
>
> In most cases you can use the **ReLU** activation function in the hidden layers (or one of its variants). It is a bit faster to compute than other activation functions, and Gradient Descent does not get stuck as much on plateaus, thanks to the fact that it does not saturate for large input values (as opposed to the logistic function or the hyperbolic tangent function, which saturates at 1).
>
> For the output layer, the **softmax** activation function is generally a good choice for classification tasks when the classes are mutually exclusive. When they are not mutually exclusive (or when there are just two classes), you generally want to use the **logistic** function function. For regression tasks, you can simply use **no activation function** at all for the output layer.



Additional notes:

| Loss function         | Metric | Notes                                                        |
| --------------------- | ------ | ------------------------------------------------------------ |
| `binary_crossentropy` | `acc`  | Be sure to use `class_mode='binary'` when using a generator in binary classification. From [1], page 151: *Because you use `binary_crossentropy` loss, you need binary labels*. |
|                       |        |                                                              |
|                       |        |                                                              |
|                       |        |                                                              |
|                       |        |                                                              |
|                       |        |                                                              |
|                       |        |                                                              |
|                       |        |                                                              |
|                       |        |                                                              |











## Binary classification

[colocar ejemplo completo] para mostrar si se usa o no one-hot encoding







## References

[1] Deep learning with Python

[2] Hands-On Machine Learning with Scikit-Learn & Tensorflow